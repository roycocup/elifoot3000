function play() {
    Preferenze = salva_preferenze();
    Settings = get_game_setting();
    phaser_init()
}

function salva_preferenze() {
    var e = document.getElementById("preferenze_video_proporzioni").value;
    var t = document.getElementById("preferenze_video_zoom").value;
    var n = document.getElementById("preferenze_audio_sounds").checked;
    var r = document.getElementById("preferenze_gioco_durata").value;
    var i = {};
    i.video = {};
    i.video.proporzioni = e;
    i.video.zoom = parseInt(t);
    i.audio = {};
    i.audio.sounds = n;
    i.gioco = {};
    i.gioco.durata = r;
    salva_dato("Preferenze", JSON.stringify(i));
    return i
}

function get_game_setting() {
    var e = document.getElementById("settings_squadra1").value;
    var t = document.getElementById("settings_squadra2").value;
    var n = {};
    n.Players = new Array;
    n.Squadre = new Array;
    for (var r = 1; r <= 2; r++) {
        n.Players[r] = {};
        n.Players[r].keylayout = r;
        if (r == e) n.Players[r].squadra = 1;
        else if (r == t) n.Players[r].squadra = 2;
        else n.Players[r].squadra = 0;
        n.Squadre[r] = {};
        n.Squadre[r].id = document.getElementById("settings_sel_squadra" + r).value
    }
    return n
}

function update_gameconfvars() {
    var e = Preferenze.video.proporzioni.split(":");
    var t = [Preferenze.video.zoom, Preferenze.video.zoom * e[1] / e[0]];
    gameconf.screenW = t[0];
    gameconf.screenH = t[1];
    gameconf.durataPartita = Preferenze.gioco.durata * 60;
    return true
}

function phaser_init() {
    if (update_gameconfvars()) {
        game = new Phaser.Game(gameconf.screenW, gameconf.screenH, Phaser.CANVAS, "gamearea", {
            preload: preload,
            create: create,
            update: update,
            render: render
        })
    } else alert("Preferences error")
}

function preload() {
    game_preload();
    game.stage.disableVisibilityChange = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    game.scale.setScreenSize(true);
    document.getElementById("welcome").style.display = "none";
    document.getElementById("loading").style.display = "block";
    game.load.spritesheet("portiere", "assets/sprites/portiere.png", gameconf.ominoW, gameconf.ominoH);
    game.load.spritesheet("arbitro", "assets/sprites/arbitro.png", gameconf.ominoW, gameconf.ominoH);
    game.load.spritesheet("omino1", "assets/sprites/player.png", gameconf.ominoW, gameconf.ominoH);
    game.load.spritesheet("omino2", "assets/sprites/player2.png", gameconf.ominoW, gameconf.ominoH);
    game.load.spritesheet("palo", "assets/sprites/palo.png", gameconf.paloR * 2, gameconf.paloR * 2);
    game.load.spritesheet("ball", "assets/sprites/ball.png", gameconf.pallaW, gameconf.pallaH, 7);
    game.load.spritesheet("activePCursor", "assets/sprites/activeP.png");
    game.load.image("campo", "assets/bg/campo.png");
    game.load.audio("stadio", ["assets/audio/stadioLungo.mp3"]);
    game.load.audio("goal1", ["assets/audio/goal1.mp3"]);
    game.load.audio("goal2", ["assets/audio/goal2.mp3"]);
    game.load.audio("ohh1", ["assets/audio/ohh1.mp3"]);
    game.load.audio("fischi", ["assets/audio/fischi.mp3"]);
    game.load.audio("coro1", ["assets/audio/coro1.mp3"])
}

function create() {
    motore_init();
    document.getElementById("loading").style.display = "none";
    suoni["stadio"] = game.add.audio("stadio");
    play_sound("stadio", true);
    suoni["goal1"] = game.add.audio("goal1");
    suoni["goal2"] = game.add.audio("goal2");
    suoni["ohh1"] = game.add.audio("ohh1");
    suoni["fischi"] = game.add.audio("fischi");
    suoni["coro1"] = game.add.audio("coro1");
    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.defaultRestitution = .8;
    game.physics.p2.setImpactEvents(true);
    crea_campo();
    oggetti.calciatoriCollisionGroup = game.physics.p2.createCollisionGroup();
    oggetti.pallaCollisionGroup = game.physics.p2.createCollisionGroup();
    oggetti.staticiCollisionGroup = game.physics.p2.createCollisionGroup();
    game.physics.p2.updateBoundsCollisionGroup();
    set_sprites_and_animations();
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    tasti[1] = {};
    tasti[1].cursors = game.input.keyboard.createCursorKeys();
    tasti[1].tiro = game.input.keyboard.addKey(Phaser.Keyboard.I);
    tasti[1].passaggio = game.input.keyboard.addKey(Phaser.Keyboard.O);
    tasti[1].passaggio2 = game.input.keyboard.addKey(Phaser.Keyboard.P);
    tasti[1].passaggio.onDown.add(premitasto, this);
    tasti[1].passaggio.onDown.add(premitastoPassP1, this);
    tasti[1].passaggio2.onDown.add(premitasto, this);
    tasti[1].passaggio2.onDown.add(premitastoPass2P1, this);
    tasti[1].tiro.onDown.add(premitasto, this);
    tasti[1].tiro.onDown.add(premitastoTiroP1, this);
    tasti[2] = {};
    tasti[2].cursors = {};
    tasti[2].cursors.up = game.input.keyboard.addKey(Phaser.Keyboard.W);
    tasti[2].cursors.right = game.input.keyboard.addKey(Phaser.Keyboard.D);
    tasti[2].cursors.down = game.input.keyboard.addKey(Phaser.Keyboard.S);
    tasti[2].cursors.left = game.input.keyboard.addKey(Phaser.Keyboard.A);
    tasti[2].tiro = game.input.keyboard.addKey(Phaser.Keyboard.C);
    tasti[2].passaggio = game.input.keyboard.addKey(Phaser.Keyboard.V);
    tasti[2].passaggio2 = game.input.keyboard.addKey(Phaser.Keyboard.B);
    tasti[2].passaggio.onDown.add(premitasto, this);
    tasti[2].passaggio.onDown.add(premitastoPassP2, this);
    tasti[2].passaggio2.onDown.add(premitasto, this);
    tasti[2].passaggio2.onDown.add(premitastoPass2P2, this);
    tasti[2].tiro.onDown.add(premitasto, this);
    tasti[2].tiro.onDown.add(premitastoTiroP2, this);
    tasti[0] = {};
    tasti[0].pausa = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    tasti[0].pausa.onDown.add(gamePausa, this);
    tasti[0].skippa = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    tasti[0].skippa.onDown.add(skippa, this);
    tasti[0].PgSu = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_UP);
    tasti[0].PgGiu = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_DOWN);
    tasti[0].Inizio = game.input.keyboard.addKey(Phaser.Keyboard.HOME);
    tasti[0].Fine = game.input.keyboard.addKey(Phaser.Keyboard.END);
    game.camera.follow(oggetti.palla);
    camera_set_deadzone();
    game_init()
}

function update() {
    if (gamevars.gamestate == 1) motore();
    else if (gamevars.gamestate == 2) scena();
    else {
        game.paused = true;
        console.log("gamestate = " + gamevars.gamestate);
        return false
    }
    movimento_palla();
    movimento_arbitri();
    var e;
    for (var t = 1; t <= 2; t += 1) {
        e = gamevars.Squadre[t].activePlayer;
        if (typeof e !== "undefined" && e !== false) {
            oggetti.activePSprite[t].x = oggetti.calciatori[e].body.x;
            oggetti.activePSprite[t].y = oggetti.calciatori[e].body.y
        }
    }
    if (tasti[0].PgSu.isDown) {
        game.camera.follow(null);
        game.camera.y -= 20
    } else if (tasti[0].PgGiu.isDown) {
        game.camera.follow(null);
        game.camera.y += 20
    }
    if (tasti[0].Inizio.isDown) {
        game.camera.follow(null);
        game.camera.x -= 20
    } else if (tasti[0].Fine.isDown) {
        game.camera.follow(null);
        game.camera.x += 20
    }
}

function render() {
}

function crea_campo() {
    oggetti.statici = game.add.group();
    var e = get_fuoricampo_coords();
    game.stage.backgroundColor = 5066061;
    game.add.tileSprite(0, 0, gameconf.arenaW, gameconf.arenaH, "campo");
    game.world.setBounds(0, 0, gameconf.arenaW, gameconf.arenaH);
    var t = e["y1"] + 4;
    var n = e["y2"] + 4;
    oggetti.porte = new Array;
    oggetti.porte[1] = new Phaser.Rectangle(gameconf.porteX, t, gameconf.porteW, gameconf.porteH);
    oggetti.porte[2] = new Phaser.Rectangle(gameconf.porteX, n, gameconf.porteW, gameconf.porteH);
    oggetti.aree = new Array;
    oggetti.aree[1] = new Phaser.Rectangle(gameconf.areeX, e["y1"] + 16, gameconf.areeW, gameconf.areeH);
    oggetti.aree[2] = new Phaser.Rectangle(gameconf.areeX, e["y2"] - gameconf.areeH, gameconf.areeW, gameconf.areeH);
    oggetti.pali = new Array;
    oggetti.pali[1] = new Array;
    oggetti.pali[1][1] = game.add.sprite(gameconf.porteX - gameconf.paloR, e["y1"], "palo");
    oggetti.pali[1][2] = game.add.sprite(gameconf.porteX + gameconf.paloR + gameconf.porteW, e["y1"], "palo");
    oggetti.pali[2] = new Array;
    oggetti.pali[2][1] = game.add.sprite(gameconf.porteX - gameconf.paloR, e["y2"] - gameconf.paloR * 2, "palo");
    oggetti.pali[2][2] = game.add.sprite(gameconf.porteX + gameconf.paloR + gameconf.porteW, e["y2"] - gameconf.paloR * 2, "palo");
    oggetti.statici.add(oggetti.pali[1][1]);
    oggetti.statici.add(oggetti.pali[1][2]);
    oggetti.statici.add(oggetti.pali[2][1]);
    oggetti.statici.add(oggetti.pali[2][2]);
    set_zone_campo();
    var r = {
        font: "24px Arial",
        fill: "#ffffff",
        align: "center"
    };
    oggetti.scritte = {};
    oggetti.scritte.tabellone = game.add.text(20, 20, "", r);
    oggetti.scritte.tabellone.fixedToCamera = true;
    show_score();
    oggetti.scritte.tempo = game.add.text(gameconf.screenW - 40, 20, "", r);
    oggetti.scritte.tempo.fixedToCamera = true;
    r = {
        font: "16px Arial",
        fill: "#ffffff",
        align: "center"
    };
    oggetti.scritte.giocatore = game.add.text(20, 60, "", r);
    oggetti.scritte.giocatore.fixedToCamera = true
}

function set_zone_campo() {
    var e = confTactics.quanteZoneCampoX;
    var t = confTactics.quanteZoneCampoY;
    var n, r, i, s;
    var o = 0;
    var u = get_fuoricampo_coords();
    var a = gameconf.arenaEffettiveW / e;
    var f = gameconf.arenaEffettiveH / t;
    oggetti.zone = new Array;
    for (r = 0; r < t; r++) {
        s = r * f;
        s += u["y1"];
        for (n = 0; n < e; n++) {
            i = n * a;
            i += u["x1"];
            o++;
            oggetti.zone[o] = new Phaser.Rectangle(i, s, a, f)
        }
    }
}

function checkOverlap(e, t) {
    var n = e.getBounds();
    var r = t.getBounds();
    return Phaser.Rectangle.intersects(n, r)
}

function contiene(e, t) {
    return e.contains(t.x, t.y)
}

function tiro(e, t, n) {
    var r = get_direzione(e);
    var i = e.ME.squadra;
    var s = gamevars.Squadre[i].campo;
    var o = get_fuoricampo_coords();
    var u;
    var a = false;
    if (e.ME.missione == "scivolata") return false;
    if (e.ME.ruolo == "P") {
        if (t == "tiro" && get_zonaCampo(e) == "D" || n === false) {
            t = "spazza";
            n = false
        }
    }
    if (t == "pass") u = calcola_skill_omino(e.ME.db["pass"], gameconf.passaggioSpeed, gameconf.passskillK);
    else if (t == "pass2") u = gameconf.passaggioAltoSpeed;
    else if (t == "tiro") u = calcola_skill_omino(e.ME.db["tiro"], gameconf.tiroSpeed, gameconf.tiroskillK);
    else if (t == "cross") {
        a = new Phaser.Point(oggetti.aree[get_opponent_id(s)].centerX, oggetti.aree[get_opponent_id(s)].centerY);
        u = gameconf.crossSpeed
    } else if (t == "spazza") u = calcola_skill_omino(e.ME.db["tiro"], gameconf.spazzaSpeed, gameconf.tiroskillK);
    else u = gameconf.palladefSpeed;
    if (t == "tiro") a = mira_tiro(e);
    else if (t == "spazza") a = oggetti.porte[get_opponent_id(s)];
    var f = 0;
    var l = 0;
    var c = oggetti.palla.body.x;
    var h = oggetti.palla.body.y;
    if (r.indexOf("Left") > -1) {
        f -= u;
        c = e.body.x - gameconf.ominoW
    } else if (r.indexOf("Right") > -1) {
        f += u;
        c = e.body.x + gameconf.ominoW
    }
    if (r.indexOf("Up") > -1) {
        l -= u;
        h = e.body.y - gameconf.ominoH
    } else if (r.indexOf("Down") > -1) {
        l += u;
        h = e.body.y + gameconf.ominoH
    }
    if (n === false) {
        if (a !== false) {
            var p = 5;
            if (e.ME.db["tiro"]) p -= e.ME.db["tiro"];
            lancia_verso(oggetti.palla, a, u, 0, 0, p)
        } else {
            var d = calcola_skill_omino(e.ME.db["pass"], gameconf.pallaliberaSpeed, gameconf.passskillK);
            if (f != 0) f = f / Math.abs(f) * d;
            if (l != 0) l = l / Math.abs(l) * d;
            u = get_absSpeed(f, l);
            oggetti.palla.body.velocity.x = f;
            oggetti.palla.body.velocity.y = l
        }
    } else {
        var v = 0;
        var m = 0;
        a = oggetti.calciatori[n];
        cambia_missione(a, "wait");
        lancia_verso(oggetti.palla, a, u, v, m)
    }
    if (t != "pass") {
        set_palla_doing(t, u)
    } else palla_doing_reset();
    blocca_calciatore(e, "blocco")
}

function mira_tiro(e) {
    var t, n;
    var r = get_fuoricampo_coords();
    var i = e.ME.squadra;
    var s = gamevars.Squadre[i].campo;
    var o = oggetti.porte[get_opponent_id(s)];
    if (Math.floor(Math.random() * 2 + 1) == 1) t = parseFloat(o.left) + gameconf.pallaW;
    else t = parseFloat(o.right) - gameconf.pallaW;
    if (s == 1) n = o.top;
    else n = o.bottom;
    return new Phaser.Point(t, n)
}

function resetta_missioni() {
    for (var e in oggetti.calciatori) {
        if (oggetti.calciatori[e].ME.missione == "wait") oggetti.calciatori[e].ME.missione = ""
    }
}

function cambia_missione(e, t, n) {
    if (check_inattivo(e, true)) {
        e.ME.nextmissione = t;
        if (n) {
            e.ME.nextmissioneX = n[0];
            e.ME.nextmissioneY = n[1]
        } else {
            e.ME.nextmissioneX = false;
            e.ME.nextmissioneY = false
        }
    } else {
        e.ME.timer.stop();
        e.ME.missione = t;
        if (n) {
            e.ME.missioneX = n[0];
            e.ME.missioneY = n[1]
        } else {
            if (t == "") {
                e.ME.missioneX = false;
                e.ME.missioneY = false
            }
        }
    }
}

function blocca_calciatore(e, t) {
    if (t == 0) {
        e.ME.timer.stop();
        e.ME.missione = "";
        e.ME.animazione = "";
        cambia_animazione(e, "Fermo");
        if (e.ME.nextmissione) {
            cambia_missione(e, e.ME.nextmissione, [e.ME.nextmissioneX, e.ME.nextmissioneY]);
            e.ME.nextmissione = false;
            e.ME.nextmissioneY = false;
            e.ME.nextmissioneX = false
        }
    } else {
        var n = gameconf.cusciVaiTime;
        if (e.ME.missione == "steso") n = gameconf.stesoTime;
        e.ME.timer.stop();
        e.ME.timer = game.time.create(false);
        e.ME.missione = t;
        e.ME.timer.loop(n, blocca_calciatore, this, e, 0);
        e.ME.timer.start()
    }
}

function set_sprites_and_animations() {
    var e;
    oggetti.omini = game.add.group();
    oggetti.shadow = game.add.sprite(0, 0, "ball");
    oggetti.shadow.anchor.set(.5);
    oggetti.shadow.tint = 0;
    oggetti.shadow.alpha = 0;
    oggetti.palla = game.add.sprite(0, 0, "ball");
    oggetti.activePSprite = new Array;
    oggetti.activePSprite[1] = game.add.sprite(0, 0, "activePCursor");
    oggetti.activePSprite[1].anchor.set(.5);
    oggetti.activePSprite[2] = game.add.sprite(0, 0, "activePCursor");
    oggetti.activePSprite[2].anchor.set(.5);
    oggetti.activePSprite[2].tint = 16711680;
    if (Settings.Players[1].squadra == 1 || Settings.Players[2].squadra == 1) oggetti.activePSprite[1].alpha = .5;
    else oggetti.activePSprite[1].alpha = 0;
    if (Settings.Players[1].squadra == 2 || Settings.Players[2].squadra == 2) oggetti.activePSprite[2].alpha = .5;
    else oggetti.activePSprite[2].alpha = 0;
    calciatori_crea();
    oggetti.omini.callAll("animations.add", "animations", "Fermo", [0]);
    oggetti.omini.callAll("animations.add", "animations", "runUp", [0, 1, 2, 3, 4, 5, 6, 7]);
    oggetti.omini.callAll("animations.add", "animations", "runUpRight", [8, 9, 10, 11, 12, 13, 14, 15]);
    oggetti.omini.callAll("animations.add", "animations", "runRight", [16, 17, 18, 19, 20, 21, 22, 23]);
    oggetti.omini.callAll("animations.add", "animations", "runDownRight", [24, 25, 26, 27, 28, 29, 30, 31]);
    oggetti.omini.callAll("animations.add", "animations", "runDown", [32, 33, 34, 35, 36, 37, 38, 39]);
    oggetti.omini.callAll("animations.add", "animations", "runDownLeft", [40, 41, 42, 43, 44, 45, 46, 47]);
    oggetti.omini.callAll("animations.add", "animations", "runLeft", [48, 49, 50, 51, 52, 53, 54, 55]);
    oggetti.omini.callAll("animations.add", "animations", "runUpLeft", [56, 57, 58, 59, 60, 61, 62, 63]);
    var e = new Array;
    for (var t = 140; t <= 143; t += 1) {
        e.push(t)
    }
    oggetti.omini.callAll("animations.add", "animations", "esulta", e);
    oggetti.omini.callAll("animations.add", "animations", "scivolataUp", [88]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataUpRight", [89]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataRight", [90]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataDownRight", [91]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataDown", [92]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataDownLeft", [93]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataLeft", [94]);
    oggetti.omini.callAll("animations.add", "animations", "scivolataUpLeft", [95]);
    oggetti.omini.callAll("animations.add", "animations", "stesoFermo", [148, 149, 150]);
    oggetti.omini.callAll("animations.add", "animations", "stesoUp", [148, 149, 150]);
    oggetti.omini.callAll("animations.add", "animations", "stesoUpRight", [151, 152, 153]);
    oggetti.omini.callAll("animations.add", "animations", "stesoRight", [154, 155, 156]);
    oggetti.omini.callAll("animations.add", "animations", "stesoDownRight", [157, 158, 159]);
    oggetti.omini.callAll("animations.add", "animations", "stesoDown", [160, 161, 162]);
    oggetti.omini.callAll("animations.add", "animations", "stesoDownLeft", [163, 164, 165]);
    oggetti.omini.callAll("animations.add", "animations", "stesoLeft", [166, 167, 168]);
    oggetti.omini.callAll("animations.add", "animations", "stesoUpLeft", [169, 170, 171]);
    oggetti.omini.callAll("play", null, "Fermo", gameconf.ominoframerate, true);
    oggetti.palla.animations.add("fermo", [0]);
    oggetti.palla.animations.add("vai");
    oggetti.palla.animations.play("fermo");
    game.physics.p2.enable([oggetti.omini, oggetti.palla, oggetti.statici]);
    oggetti.statici.setAll("body.static", true);
    oggetti.statici.setAll("body.debug", gameconf.bodyDebug);
    gamevars.flags.paliColl = true;
    pali_clearShapes();
    pali_setShapes();
    oggetti.omini.setAll("body.damping", 0);
    oggetti.omini.setAll("body.fixedRotation", 1);
    oggetti.omini.setAll("body.debug", gameconf.bodyDebug);
    oggetti.omini.callAll("body.clearShapes", "body");
    oggetti.omini.callAll("body.setCircle", "body", gameconf.ominoW / 2, 0, 0);
    oggetti.palla.body.damping = gameconf.attritoTerra;
    oggetti.palla.body.fixedRotation = true;
    oggetti.palla.body.debug = gameconf.bodyDebug;
    oggetti.palla.ME = {
        doing: "",
        z: 0,
        timer: game.time.create(false),
        timerScena: game.time.create(false)
    };
    oggetti.palla.scale.x = oggetti.palla.scale.y = gameconf.pallaScale;
    oggetti.palla.body.clearShapes();
    oggetti.palla.body.setCircle(gameconf.pallaRaggio / 2);
    oggetti.palla.body.setCollisionGroup(oggetti.pallaCollisionGroup);
    oggetti.omini.callAll("body.setCollisionGroup", "body", oggetti.calciatoriCollisionGroup);
    oggetti.statici.callAll("body.setCollisionGroup", "body", oggetti.staticiCollisionGroup);
    oggetti.palla.body.collides([oggetti.staticiCollisionGroup]);
    oggetti.statici.callAll("body.collides", "body", [oggetti.pallaCollisionGroup])
}

function _clearShapes(e) {
    e.body.clearShapes()
}

function _setCircle(e, t) {
    e.body.setCircle(t)
}

function pali_clearShapes() {
    if (gamevars.flags.paliColl) {
        gamevars.flags.paliColl = false;
        _clearShapes(oggetti.pali[1][1]);
        _clearShapes(oggetti.pali[1][2]);
        _clearShapes(oggetti.pali[2][1]);
        _clearShapes(oggetti.pali[2][2])
    }
}

function pali_setShapes() {
    if (!gamevars.flags.paliColl) {
        gamevars.flags.paliColl = true;
        _setCircle(oggetti.pali[1][1], gameconf.paloR * 2);
        _setCircle(oggetti.pali[1][2], gameconf.paloR * 2);
        _setCircle(oggetti.pali[2][1], gameconf.paloR * 2);
        _setCircle(oggetti.pali[2][2], gameconf.paloR * 2)
    }
}

function gofull() {
    if (game.scale.isFullScreen) {
        game.scale.stopFullScreen()
    } else {
        game.scale.startFullScreen(false)
    }
}

function camera_set_deadzone() {
    game.camera.deadzone = new Phaser.Rectangle(gameconf.screenW / 3, gameconf.screenH * 3 / 8, gameconf.screenW / 3, gameconf.screenH / 4)
}

function movimento_omino(e, t) {
    var n = false;
    if (t.ME.missione != "") esegui_missione(t);
    else {
        if (userTeam(t) == 0) {
            if (gameconf.DebugStop) return false;
            else n = azioni_AI(t)
        }
        if (!n) {
            var r = calciatore_cerca_posizione(e, t);
            var i = r[0];
            var s = r[1];
            var o = new Phaser.Point(i, s);
            vaiverso(t, o)
        }
    }
}

function scivolata(e) {
    if (e.ME.missione != "") return false;
    var t = calcola_skill_omino(e.ME.db["tackle"], gameconf.scivolataVelMolt, gameconf.tackleskillK);
    e.body.velocity.x = e.body.velocity.x * t;
    e.body.velocity.y = e.body.velocity.y * t;
    e.body.damping = gameconf.scivolataAttrito;
    e.ME.animazione = "scivolata" + get_direzione(e);
    cambia_missione(e, "scivolata")
}

function esegui_missione(e) {
    if (check_inattivo(e)) {
        e.body.setZeroVelocity()
    } else if (e.ME.missione == "wait") {
        var t = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
        vaiverso(e, t)
    } else if (e.ME.missione == "rientra") {
        var t = new Phaser.Point(e.ME.missioneX, e.ME.missioneY);
        if (t.distance(e) <= gameconf.cusciVai) {
            cambia_missione(e, "")
        } else {
            vaiverso(e, t)
        }
    } else if (e.ME.missione == "scivolata") {
        var n = parseInt(get_absSpeed(e.body.velocity.x, e.body.velocity.y));
        if (n < gameconf.scivolataVelStop) {
            blocca_calciatore(e, "");
            e.ME.animazione = "";
            e.body.setZeroDamping();
            e.body.setZeroVelocity();
            cambia_animazione(e, "Fermo")
        } else {
            if (gamevars.scena == 0) {
                var r;
                for (var i in oggetti.calciatori) {
                    r = oggetti.calciatori[i];
                    if (r.ME.id == e.ME.id || r.ME.missione == "steso" || r.ME.missione == "scivolata") continue;
                    if (checkOverlap(e, r)) {
                        giocatore_colpito(r);
                        if (check_fallo(e, r)) fallo(e, r)
                    }
                }
            }
        }
    }
}

function giocatore_colpito(e) {
    e.body.setZeroVelocity();
    e.ME.animazione = "skip";
    cambia_missione(e, "steso");
    blocca_calciatore(e, "steso");
    e.animations.play("steso" + get_direzione(e), gameconf.ominoframerate, false)
}

function check_fallo(e, t) {
    if (gamevars.scena > 0 || e.ME.squadra == t.ME.squadra) return false;
    if (!e.ME.halapalla) {
        return true
    } else return false
}

function fallo(e, t) {
    tifosi("fallo", e.ME.squadra);
    gamevars.possesso = t.ME.squadra;
    var n = t.body.x;
    var r = t.body.y;
    oggetti.palla.ME.timerScena.stop();
    oggetti.palla.ME.timerScena.add(1e3, piazza_palla, this, n, r);
    oggetti.palla.ME.timerScena.start();
    set_scena(7, new Phaser.Point(n, r))
}

function check_inattivo(e, t) {
    if (t) return e.ME.missione == "blocco" || e.ME.missione == "steso" || e.ME.missione == "scivolata";
    else return e.ME.missione == "blocco" || e.ME.missione == "steso"
}

function controlla_omino() {
    var e;
    for (var t = 1; t <= 2; t += 1) {
        e = Settings.Players[t].squadra;
        if (e == 0) continue;
        var n = oggetti.calciatori[gamevars.Squadre[e].activePlayer];
        if (check_inattivo(n)) {
            n.body.setZeroVelocity();
            return false
        }
        if (n.ME.missione == "scivolata") {
            esegui_missione(n);
            return false
        }
        if (n.ME.halapalla && n.ME.inmano) {
            n.body.setZeroVelocity();
            return false
        }
        n.body.setZeroVelocity();
        var r = get_direzione_utente(Settings.Players[t].keylayout);
        var i = get_omino_speed(n);
        if (n.ME.halapalla) i = i * gameconf.controlloattrito;
        if ((r.indexOf("Left") > -1 || r.indexOf("Right") > -1) && (r.indexOf("Up") > -1 || r.indexOf("Down") > -1)) i = i / Math.sqrt(2);
        if (r.indexOf("Left") > -1) n.body.moveLeft(i);
        else if (r.indexOf("Right") > -1) n.body.moveRight(i);
        if (r.indexOf("Up") > -1) n.body.moveUp(i);
        else if (r.indexOf("Down") > -1) n.body.moveDown(i)
    }
}

function disable_oggetti_bassi(e) {
    if (e > gameconf.porteZ) pali_clearShapes();
    else pali_setShapes()
}

function movimento_palla() {
    oggetti.shadow.x = oggetti.palla.body.x + 2;
    oggetti.shadow.y = oggetti.palla.body.y + 3;
    var e = get_absSpeed(oggetti.palla.body.velocity.x, oggetti.palla.body.velocity.y);
    if (oggetti.palla.ME.doing != "") {
        var t = get_z();
        var n = 0;
        if (t == 0 && t == oggetti.palla.ME.z) n = 0;
        else if (t > oggetti.palla.ME.z) n = 1;
        else if (t <= oggetti.palla.ME.z) n = -1;
        oggetti.palla.ME.z = t;
        if (oggetti.palla.ME.z > 0) {
            if (oggetti.palla.body.damping != gameconf.attritoAria) oggetti.palla.body.damping = gameconf.attritoAria;
            var r = oggetti.palla.ME.z / 100;
            oggetti.palla.scale.x = oggetti.palla.scale.y = gameconf.pallaScale + r * gameconf.pallaScale;
            oggetti.palla.bringToTop()
        } else {
            if (oggetti.palla.body.damping != gameconf.attritoTerra) oggetti.palla.body.damping = gameconf.attritoTerra;
            oggetti.palla.scale.x = oggetti.palla.scale.y = gameconf.pallaScale;
            if (n < 0 && e > 100) {
                e = e * gameconf.attritoRimbalzo;
                oggetti.palla.body.velocity.x = oggetti.palla.body.velocity.x * gameconf.attritoRimbalzo;
                oggetti.palla.body.velocity.y = oggetti.palla.body.velocity.y * gameconf.attritoRimbalzo;
                set_palla_doing(oggetti.palla.ME.doing, e)
            }
        }
    } else {
        if (oggetti.palla.body.damping != gameconf.attritoTerra) oggetti.palla.body.damping = gameconf.attritoTerra
    }
    disable_oggetti_bassi(oggetti.palla.ME.z);
    if (parseInt(e) > 0) {
        oggetti.palla.animations.play("vai", parseInt(e) / 10, true);
        oggetti.palla.animations.getAnimation("vai").delay = 3e3 / parseInt(e)
    } else {
        oggetti.palla.animations.play("fermo")
    }
    for (var i in oggetti.zone) {
        if (contiene(oggetti.zone[i], oggetti.palla)) {
            oggetti.palla.ME.zona = i;
            break
        }
    }
}

function set_palla_doing(e, t) {
    if (oggetti.palla.ME.doing != "") oggetti.palla.ME.timer.stop();
    oggetti.palla.ME.doing = e;
    oggetti.palla.ME.v0 = t;
    oggetti.palla.ME.z = 0;
    oggetti.palla.ME.timer = game.time.create(false);
    oggetti.palla.ME.timer.start()
}

function palla_doing_reset() {
    if (oggetti.palla.ME.doing != "") {
        oggetti.palla.ME.timer.stop();
        oggetti.palla.ME.doing = "";
        oggetti.palla.ME.z = 0;
        oggetti.palla.ME.v0 = 0;
        oggetti.palla.scale.x = oggetti.palla.scale.y = gameconf.pallaScale
    }
}

function cambia_animazione(e, t) {
    if (t == "runFermo") e.animations.stop();
    else e.animations.play(t, gameconf.ominoframerate, true)
}

function userTeam(e) {
    for (var t in Settings.Players) {
        if (Settings.Players[t].squadra == e.ME.squadra) return t
    }
    return 0
}

function lancia_verso(e, t, n, r, i, s) {
    if (typeof e.centerX !== "undefined") var o = e.centerX;
    else var o = e.x;
    if (typeof e.centerY !== "undefined") var u = e.centerY;
    else var u = e.y;
    if (typeof t.centerX !== "undefined") var a = t.centerX;
    else var a = t.x;
    if (typeof t.centerY !== "undefined") var f = t.centerY;
    else var f = t.y;
    if (typeof r !== "undefined") {
        a += r
    }
    if (typeof i !== "undefined") {
        f += i
    }
    var l = a - o;
    var c = f - u;
    var h = get_diagonale(l, c);
    if (s && s > 0) {
        var p = Math.floor(Math.random() * parseInt(h / 8) + 1) * s;
        var d = Math.floor(Math.random() * 2 + 1);
        if (d == 1) p = p * -1;
        a += p;
        l = a - o;
        c = f - u;
        h = get_diagonale(l, c)
    }
    var v = n * l / h;
    var m = n * c / h;
    e.body.velocity.x = v;
    e.body.velocity.y = m
}

function get_omino_speed(e) {
    var t = gameconf.ominospeed;
    if (e.ME.db) t = calcola_skill_omino(e.ME.db["speed"], gameconf.ominospeed, gameconf.speedskillK);
    else if (e.ME.speed) t = e.ME.speed;
    return t
}

function calcola_skill_omino(e, t, n) {
    if (isNaN(parseInt(e))) e = 0;
    else e++;
    return t + n * e
}

function vaiverso(e, t) {
    var n = get_omino_speed(e);
    if (gamevars.gamestate > 1) {
        if (gamevars.scena == 1 || gamevars.scena == 2) n = n * 3 / 2
    }
    var r = t.distance(e);
    var i = t.x - e.x;
    var s = t.y - e.y;
    if (e.ME.halapalla) n = n * gameconf.controlloattrito;
    e.body.setZeroVelocity();
    if (Math.abs(i) > gameconf.cusciVai && Math.abs(s) > gameconf.cusciVai) n = n / Math.sqrt(2);
    if (i < 0) {
        if (Math.abs(i) > gameconf.cusciVai) e.body.moveLeft(n)
    }
    if (i > 0) {
        if (Math.abs(i) > gameconf.cusciVai) e.body.moveRight(n)
    }
    if (s < 0) {
        if (Math.abs(s) > gameconf.cusciVai) e.body.moveUp(n)
    }
    if (s > 0) {
        if (Math.abs(s) > gameconf.cusciVai) e.body.moveDown(n)
    }
    if (Math.abs(i) <= gameconf.cusciVai && Math.abs(s) <= gameconf.cusciVai) {
        e.body.setZeroVelocity();
        e.body.x = t.x;
        e.body.y = t.y;
        e.ME.animazione = "";
        cambia_animazione(e, "Fermo");
        if (gamevars.scena > 0 && e.ME.missione != "") scena_do(e)
    }
}

function get_direzione(e) {
    if (e.body.velocity.y < 0 && e.body.velocity.x > 0) return "UpRight";
    else if (e.body.velocity.y < 0 && e.body.velocity.x < 0) return "UpLeft";
    else if (e.body.velocity.y > 0 && e.body.velocity.x > 0) return "DownRight";
    else if (e.body.velocity.y > 0 && e.body.velocity.x < 0) return "DownLeft";
    else if (e.body.velocity.x < 0) return "Left";
    else if (e.body.velocity.x > 0) return "Right";
    else if (e.body.velocity.y < 0) return "Up";
    else if (e.body.velocity.y > 0) return "Down";
    else return "Fermo"
}

function get_direzione_utente(e) {
    if (tasti[e].cursors.up.isDown && tasti[e].cursors.right.isDown) return "UpRight";
    else if (tasti[e].cursors.up.isDown && tasti[e].cursors.left.isDown) return "UpLeft";
    else if (tasti[e].cursors.down.isDown && tasti[e].cursors.right.isDown) return "DownRight";
    else if (tasti[e].cursors.down.isDown && tasti[e].cursors.left.isDown) return "DownLeft";
    else if (tasti[e].cursors.left.isDown) return "Left";
    else if (tasti[e].cursors.right.isDown) return "Right";
    else if (tasti[e].cursors.up.isDown) return "Up";
    else if (tasti[e].cursors.down.isDown) return "Down";
    else return "Fermo"
}

function get_absSpeed(e, t) {
    return Math.sqrt(Math.pow(Math.abs(e), 2) + Math.pow(Math.abs(t), 2))
}

function get_diagonale(e, t) {
    return Math.sqrt(Math.pow(e, 2) + Math.pow(t, 2))
}

function get_z() {
    if (oggetti.palla.ME.doing == "tiro") e = 1 / 2;
    else if (oggetti.palla.ME.doing == "cross") e = .8;
    else var e = 4 / 3;
    var t = oggetti.palla.ME.v0;
    var n = t * Math.sin(45) * e;
    var r = 9.80665 * 75;
    var i = oggetti.palla.ME.timer.ms / 1e3;
    var s = n * i - r * Math.pow(i, 2) / 2;
    if (s < 0) s = 0;
    return s
}

function cerca_omino(e, t) {
    if (typeof t === "undefined") t = 0;
    var n = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
    var r = ret = appoDist = false;
    var i = new Array;
    var s = userTeam(e);
    if (s > 0) var o = get_direzione_utente(Settings.Players[s].keylayout);
    else var o = get_direzione(e);
    for (var u in oggetti.calciatori) {
        if (u == gamevars.Squadre[e.ME.squadra].activePlayer || e.ME.squadra != oggetti.calciatori[u].ME.squadra) continue;
        if (check_inattivo(oggetti.calciatori[u])) continue;
        if (t != 1) {
            if (o.indexOf("Down") > -1 && oggetti.calciatori[u].body.y <= e.body.y) continue;
            if (o.indexOf("Up") > -1 && oggetti.calciatori[u].body.y >= e.body.y) continue;
            if (o.indexOf("Left") > -1 && oggetti.calciatori[u].body.x >= e.body.x) continue;
            if (o.indexOf("Right") > -1 && oggetti.calciatori[u].body.x <= e.body.x) continue
        }
        appoDist = n.distance(oggetti.calciatori[u]);
        if (userTeam(oggetti.calciatori[u]) > 0) {
            var a = n.x - oggetti.calciatori[u].body.x;
            var f = n.y - oggetti.calciatori[u].body.y;
            if ((o == "Down" || o == "Up") && Math.abs(f) < Math.abs(a)) continue;
            else if ((o == "Left" || o == "Right") && Math.abs(a) < Math.abs(f)) continue;
            else if ((o.indexOf("Down") > -1 || o.indexOf("Up") > -1) && Math.abs(f) < Math.abs(a) / 2) continue
        } else {
            if (appoDist < gameconf.minDistPass) continue
        }
        if (r === false || appoDist < r) {
            ret = u;
            r = appoDist
        }
        i[u] = appoDist
    }
    return ret
}

function accelerateToObject(e, t, n) {
    if (typeof n === "undefined") {
        n = 60
    }
    var r = Math.atan2(t.y - e.y, t.x - e.x);
    e.body.rotation = r + game.math.degToRad(90);
    e.body.force.x = Math.cos(r) * n;
    e.body.force.y = Math.sin(r) * n
}

function changeVolume(e) {
    for (var t in suoni) {
        if (e == "pause") {
            suoni[t].pause()
        } else if (e == "resume") {
            suoni[t].resume()
        } else if (e == 1) {
            suoni[t].volume += .1
        } else {
            suoni[t].volume -= .1
        }
    }
}

function check_halapalla(e) {
    ret = checkOverlap(e, oggetti.palla);
    if (ret) {
        if (!check_inattivo(e) && oggetti.palla.ME.z < gameconf.ominoZ) {
            if (e.ME.id != gamevars.lastContactID) {
                var t = oggetti.calciatori[get_i_from_id(gamevars.lastContactID)];
                if (t.ME.halapalla && e.ME.squadra != t.ME.squadra) giocatore_colpito(t);
                palla_justgot(e, t)
            }
            gamevars.lastContactID = e.ME.id
        } else ret = false
    }
    return ret
}

function check_incampo(e) {
    var t = get_fuoricampo_coords();
    if (e.y < t["y1"] - e.height / 2) return 1;
    else if (e.y > t["y2"] + e.height / 2) return 3;
    else if (e.x < t["x1"] - e.width / 2) return 4;
    else if (e.x > t["x2"] + e.width / 2) return 2;
    else return false
}

function get_i_from_id(e) {
    for (var t in oggetti.calciatori) {
        if (oggetti.calciatori[t].ME.id == e) return t
    }
    return false
}

function palla_uscita(e) {
    var t = get_fuoricampo_coords();
    var n = get_i_from_id(gamevars.lastContactID);
    var r = oggetti.calciatori[n];
    var i = r.ME.squadra;
    var s = gamevars.Squadre[i].campo;
    var o = oggetti.palla.body.x;
    var u = oggetti.palla.body.y;
    var a = undefined;
    var f = 0;
    gamevars.possesso = get_opponent_id(r.ME.squadra);
    var l = gamevars.Squadre[gamevars.possesso].campo;
    if (e == 1 && s == 1) {
        if (oggetti.palla.body.x < oggetti.porte[1].centerX) o = t["x1"];
        else o = t["x2"];
        u = t["y1"];
        f = 4
    } else if (e == 3 && s == 2) {
        if (oggetti.palla.body.x < oggetti.porte[2].centerX) o = t["x1"];
        else o = t["x2"];
        u = t["y2"];
        f = 4
    } else {
        if (e == 1) {
            u = t["y1"] + gameconf.areaPortiereH;
            a = oggetti.aree[1];
            f = 3
        } else if (e == 3) {
            u = t["y2"] - gameconf.areaPortiereH;
            a = oggetti.aree[2];
            f = 3
        } else if (e == 4) {
            o = t["x1"];
            f = 5
        } else if (e == 2) {
            o = t["x2"];
            f = 5
        }
        if (f == 3) {
            if (o < t["MedX"]) o = gameconf.areaPortiereX;
            else o = gameconf.areaPortiereX + gameconf.areaPortiereW
        }
    }
    oggetti.palla.ME.timerScena.stop();
    oggetti.palla.ME.timerScena.add(1e3, piazza_palla, this, o, u);
    oggetti.palla.ME.timerScena.start();
    var c = new Phaser.Point(o, u);
    set_scena(f, c, a)
}

function piazza_palla(e, t) {
    oggetti.palla.body.setZeroVelocity();
    oggetti.palla.body.x = e;
    oggetti.palla.body.y = t
}

function gamePausa() {
    if (game.paused) {
        game.paused = false
    } else {
        game.paused = true
    }
}

function verifica_gol(e) {
    var t = false;
    var n = false;
    if (e == 1 && oggetti.palla.ME.z < gameconf.porteZ && oggetti.palla.body.x > oggetti.porte[1].x && oggetti.palla.body.x < oggetti.porte[1].x + oggetti.porte[1].width) t = 2;
    else if (e == 3 && oggetti.palla.ME.z < gameconf.porteZ && oggetti.palla.body.x > oggetti.porte[2].x && oggetti.palla.body.x < oggetti.porte[2].x + oggetti.porte[2].width) t = 1;
    if (t && t == gamevars.Squadre[1].campo) n = 1;
    else if (t && t == gamevars.Squadre[2].campo) n = 2;
    return n
}

function play_sound(e, t, n) {
    if (typeof t === "undefined") t = false;
    if (typeof n === "undefined") n = false;
    if (Preferenze.audio.sounds) suoni[e].play("", 0, 1, t, n)
}

function intersection(e, t, n, r, i) {
    var s = 0;
    var o = 1;
    var u = 2;
    var a = 3;
    var f = e.end.x - e.start.x;
    var l = e.end.y - e.start.y;
    var c = Math.sqrt(f * f + l * l);
    var h = f / c;
    var p = l / c;
    var d = h * (t.x - e.start.x) + p * (t.y - e.start.y);
    var v = d * h + e.start.x;
    var m = d * p + e.start.y;
    var g = Math.sqrt((v - t.x) * (v - t.x) + (m - t.y) * (m - t.y));
    if (g < t.radius) {
        var y = Math.sqrt(t.radius * t.radius - g * g);
        var b = h * (e.end.x - e.start.x) + p * (e.end.y - e.start.y);
        if (i) {
            if ((d - y < 0 || d - y > b) && (d + y < 0 || d + y > b)) {
                return s
            } else if (d - y < 0 || d - y > b) {
                n.x = (d + y) * h + e.start.x;
                n.y = (d + y) * p + e.start.y;
                return u
            } else if (d + y < 0 || d + y > b) {
                n.x = (d - y) * h + e.start.x;
                n.y = (d - y) * p + e.start.y;
                return u
            }
        }
        n.x = (d - y) * h + e.start.x;
        n.y = (d - y) * p + e.start.y;
        r.x = (d + y) * h + e.start.x;
        r.y = (d + y) * p + e.start.y;
        return o
    } else if (g == t.radius) {
        n.x = v;
        n.y = m;
        r.x = v;
        r.y = m;
        return a
    }
    return s
}

function premitasto() {
    if (gamevars.gamestate != 1 && gamevars.scena > 0) skippa()
}

function premitastoTiro(e) {
    var t = Settings.Players[e].squadra;
    if (typeof t === "undefined" || t == 0) return false;
    var n = oggetti.calciatori[gamevars.Squadre[t].activePlayer];
    if (gamevars.gamestate != 1) {
        if (n.ME.missione == "kickdo") {
            calcio_da_fermo(n, "tiro")
        }
        return false
    }
    if (n.ME.halapalla) tiro(n, "tiro", false);
    else {
        scivolata(n)
    }
}

function premitastoPass(e) {
    var t = Settings.Players[e].squadra;
    if (typeof t === "undefined" || t == 0) return false;
    var n = oggetti.calciatori[gamevars.Squadre[t].activePlayer];
    if (gamevars.gamestate != 1) {
        if (n.ME.missione == "kickdo") {
            calcio_da_fermo(n, "pass")
        }
        return false
    }
    if (!n.ME.halapalla) return false;
    var r = cerca_omino(n);
    tiro(n, "pass", r)
}

function premitastoPass2(e) {
    var t = Settings.Players[e].squadra;
    if (typeof t === "undefined" || t == 0) return false;
    var n = oggetti.calciatori[gamevars.Squadre[t].activePlayer];
    if (gamevars.gamestate != 1) {
        if (n.ME.missione == "kickdo") {
            calcio_da_fermo(n, "pass2")
        }
        return false
    }
    if (!n.ME.halapalla) return false;
    if (isZonaCross(n)) {
        tiro(n, "cross", false)
    } else {
        var r = cerca_omino(n);
        tiro(n, "pass2", r)
    }
}

function premitastoTiroP1() {
    premitastoTiro(1)
}

function premitastoPassP1() {
    premitastoPass(1)
}

function premitastoPass2P1() {
    premitastoPass2(1)
}

function premitastoTiroP2() {
    premitastoTiro(2)
}

function premitastoPassP2() {
    premitastoPass(2)
}

function premitastoPass2P2() {
    premitastoPass2(2)
}

function game_preload() {
}

function game_init() {
    calciatori_init();
    scorri_tempo();
    gamevars.timerUpdSmall = game.time.create(false);
    gamevars.timerUpdSmall.loop(1e3, update_small, this);
    gamevars.timerUpdSmall.start();
    set_scena(1)
}

function get_db_calciatori(e, t) {
    var n;
    var r = new Array;
    r[1] = new Array;
    r[2] = new Array;
    for (var i in Database["giocatori"]) {
        n = Database["giocatori"][i];
        if (n["squadra"] == e) r[1].push(n);
        else if (n["squadra"] == t) r[2].push(n)
    }
    return r
}

function calciatori_crea() {
    var e = get_db_calciatori(Settings.Squadre[1].id, Settings.Squadre[2].id);
    oggetti.calciatori = [];
    oggetti.arbitri = [];
    var t;
    var n = 0;
    var r, i, s, o, u, a;
    for (var f = 1; f <= 2; f += 1) {
        u = gamevars.Squadre[f].campo;
        if (u == 1) i = -32;
        else i = 32;
        r = 0;
        for (t = 0; t < gameconf.nCalcia; t += 1) {
            n++;
            s = gamevars.Squadre[f].formazione[t];
            if (s == "P") o = "portiere";
            else o = "omino" + f;
            appo = game.add.sprite(r += 32, gameconf.arenaH / 2 + i, o);
            a = Math.floor(Math.random() * 10 + 1);
            if (Math.floor(Math.random() * 2) == 0) a = a * -1;
            appo.ME = {
                id: n,
                squadra: f,
                ruolo: s,
                db: e[f][t]
            };
            oggetti.calciatori.push(appo);
            oggetti.omini.add(appo)
        }
    }
    for (t = 0; t < 1; t += 1) {
        o = "arbitro";
        appo = game.add.sprite(r += 32, gameconf.arenaH / 2 + i, o);
        appo.ME = {
            id: t + 1,
            testo: "",
            speed: gameconf.arbitrospeed
        };
        oggetti.arbitri.push(appo);
        oggetti.omini.add(appo)
    }
}

function calciatori_init() {
    var e = 0;
    var t;
    for (var n in oggetti.calciatori) {
        if (n > 0 && oggetti.calciatori[n].ME.squadra != oggetti.calciatori[n - 1].ME.squadra) {
            e = 0
        }
        e += 1;
        oggetti.calciatori[n].ME.missione = "";
        oggetti.calciatori[n].ME.numero = e;
        oggetti.calciatori[n].ME.timer = game.time.create(false)
    }
}

function motore_init() {
    gamevars.possesso = 1;
    if (Settings.Players[1].squadra == 1 || Settings.Players[2].squadra == 1) var e = Tattiche[0];
    if (Settings.Players[1].squadra == 2 || Settings.Players[2].squadra == 2) var t = Tattiche[0];
    if (!e) var e = tattica_base();
    if (!t) var t = tattica_base();
    gamevars.Squadre = new Array;
    gamevars.Squadre[1] = {
        campo: 1,
        gol: 0,
        formazione: ["P", "DD", "DS", "DCD", "DCS", "CD", "CCS", "CCD", "CS", "ACD", "ACS"],
        tattica: e
    };
    gamevars.Squadre[2] = {
        campo: 2,
        gol: 0,
        formazione: ["P", "DD", "DS", "DCD", "DCS", "CD", "CCS", "CCD", "CS", "ACD", "ACS"],
        tattica: t
    };
    gamevars.casa = gameconf.casa;
    gamevars.frazionePartita = 1;
    gamevars.timerPartita = false
}

function update_small() {
    tifosi()
}

function motore() {
    var e = check_incampo(oggetti.palla);
    if (e) {
        var t = verifica_gol(e);
        if (!t) palla_uscita(e);
        else gol(t)
    }
    var n = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
    controlla_omino();
    var r, i;
    var s = false;
    var o = false;
    for (var u in oggetti.calciatori) {
        trova_informazioni_giocatori(oggetti.calciatori[u]);
        i = check_halapalla(oggetti.calciatori[u]);
        oggetti.calciatori[u].ME.halapalla = i;
        r = userTeam(oggetti.calciatori[u]);
        if (r == 0 || u != gamevars.Squadre[Settings.Players[r].squadra].activePlayer) {
            movimento_omino(u, oggetti.calciatori[u])
        }
        dettagli_omino(oggetti.calciatori[u]);
        s = calcola_offside_coords(oggetti.calciatori[u], s);
        o = calcola_active_player(n, u, oggetti.calciatori[u], o)
    }
    calcola_offside_coords2(s);
    var a = get_active_player(n, o);
    focus_omino(a);
    if (typeof gamevars.lastContactID !== "undefined") {
        u = get_i_from_id(gamevars.lastContactID);
        if (oggetti.calciatori[u].ME.halapalla) {
            controllo_palla(oggetti.calciatori[u])
        }
    }
}

function calcola_active_player(e, t, n, r) {
    var i = n.ME.squadra;
    var s = e.distance(n);
    if (r === false) {
        r = new Array;
        r[1] = false;
        r[2] = false
    }
    if (r[i] === false) {
        r[i] = new Array;
        r[i]["min_dist"] = false;
        r[i]["ret"] = false
    }
    if (r[i]["min_dist"] === false || s < r[i]["min_dist"]) {
        r[i]["ret"] = t;
        r[i]["min_dist"] = s
    }
    return r
}

function get_active_player(e, t) {
    for (Sq = 1; Sq <= 2; Sq += 1) {
        if (!t[Sq]) return false;
        if (t[Sq]["ret"] !== false && typeof gamevars.Squadre[Sq].activePlayer !== "undefined" && t[Sq]["ret"] != gamevars.Squadre[Sq].activePlayer) {
            if (t[Sq]["min_dist"] > e.distance(oggetti.calciatori[gamevars.Squadre[Sq].activePlayer]) - gameconf.cusciFocus) t[Sq]["ret"] = gamevars.Squadre[Sq].activePlayer
        }
    }
    return [t[1]["ret"], t[2]["ret"]]
}

function focus_omino(e) {
    if (!e) return false;
    for (var t = 1; t <= 2; t += 1) {
        if (typeof gamevars.Squadre[t].activePlayer !== "undefined" && e[t - 1] != gamevars.Squadre[t].activePlayer) {
            resetta_missioni()
        }
        gamevars.Squadre[t].activePlayer = e[t - 1]
    }
}

function calcola_offside_coords(e, t) {
    if (t === false) {
        var t = new Array;
        t["min_1"] = false;
        t["min_2"] = false;
        t["max_1"] = false;
        t["max_2"] = false
    }
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    if (r == 1 && (t["min_1"] === false || e.body.y <= t["min_1"])) {
        if (t["min_1"] !== false) t["min_2"] = t["min_1"];
        t["min_1"] = e.body.y
    } else if (r == 1 && (t["min_2"] === false || e.body.y <= t["min_2"])) {
        t["min_2"] = e.body.y
    }
    if (r == 2 && (t["max_1"] === false || e.body.y >= t["max_1"])) {
        if (t["max_1"] !== false) t["max_2"] = t["max_1"];
        t["max_1"] = e.body.y
    } else if (r == 2 && (t["max_2"] === false || e.body.y >= t["max_2"])) {
        t["max_2"] = e.body.y
    }
    return t
}

function calcola_offside_coords2(e) {
    var t, n;
    var r = get_fuoricampo_coords();
    t = e["min_2"];
    n = e["max_2"];
    if (t > oggetti.palla.body.y) t = oggetti.palla.body.y;
    if (n < oggetti.palla.body.y) n = oggetti.palla.body.y;
    if (t > r["MedY"]) t = r["MedY"];
    if (n < r["MedY"]) n = r["MedY"];
    gamevars.offSideVars = [t, n];
    oggetti.lineaFuorigioco1 = new Phaser.Line(0, t, gameconf.arenaW, t);
    oggetti.lineaFuorigioco2 = new Phaser.Line(0, n, gameconf.arenaW, n)
}

function controllo_palla(e) {
    if (!check_inattivo(e) && oggetti.palla.ME.z < gameconf.ominoZ && gamevars.scena == 0) {
        oggetti.palla.body.velocity.x = e.body.velocity.x;
        oggetti.palla.body.velocity.y = e.body.velocity.y;
        oggetti.palla.body.x = e.body.x;
        oggetti.palla.body.y = e.body.y;
        oggetti.omini.bringToTop(e)
    }
}

function palla_justgot(e, t) {
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    resetta_missioni();
    palla_doing_reset();
    oggetti.palla.body.setZeroVelocity();
    if (e.ME.ruolo == "P") {
        if (e.ME.squadra != t.ME.squadra && contiene(oggetti.aree[r], e)) e.ME.inmano = true;
        else e.ME.inmano = false
    }
}

function dettagli_omino(e) {
    var t = e.ME.squadra;
    if (t) var n = gamevars.Squadre[t].campo;
    if (e.ME.animazione != "skip") {
        if (typeof e.ME.animazione !== "undefined" && e.ME.animazione != "") cambia_animazione(e, e.ME.animazione);
        else {
            var r = "run";
            cambia_animazione(e, r + get_direzione(e))
        }
    }
    if (e.ME.testo) {
        e.ME.testo.x = e.x;
        e.ME.testo.y = 3 + e.y + gameconf.ominoH / 2
    }
    if (gamevars.gamestate == 1 && e.ME.halapalla) oggetti.scritte.giocatore.setText(e.ME.numero + ". " + nome_calciatore(e.ME.db["nome"]))
}

function nome_calciatore(e) {
    var t = e.split(" ");
    return t[t.length - 1]
}

function movimento_arbitri() {
    var e = oggetti.arbitri[0];
    var t, n;
    var r = get_fuoricampo_coords();
    var i = oggetti.zone[oggetti.palla.ME.zona];
    if (!i) return false;
    if (oggetti.palla.body.y > r["MedY"]) {
        n = i.top
    } else {
        n = i.bottom
    }
    if (oggetti.palla.body.x > r["MedX"]) {
        t = i.left
    } else {
        t = i.right
    }
    var s = new Phaser.Point(t, n);
    vaiverso(e, s);
    for (var o in oggetti.arbitri) {
        dettagli_omino(oggetti.arbitri[o])
    }
}

function calciatore_cerca_posizione(e, t) {
    var n;
    var r = t.ME.squadra;
    var i = gamevars.Squadre[r].campo;
    var s, o;
    if (gamevars.lastContactID) {
        var u = get_i_from_id(gamevars.lastContactID);
        var a = oggetti.calciatori[u];
        s = a.ME.squadra;
        o = gamevars.Squadre[s].campo
    } else {
        s = 0;
        o = 0
    }
    var f = userTeam(t);
    if (f == 0 && e == gamevars.Squadre[r].activePlayer) n = get_posizione_attivo(t);
    else {
        n = get_posizione_ruolo(t, false)
    }
    var l = false;
    if (a.ME.inmano) {
        l = oggetti.aree[o]
    }
    if (l !== false && (o == 1 && t.body.y < l.bottom || o == 2 && t.body.y > l.top)) {
        if (o == 1) n[1] = l.bottom;
        else n[1] = l.top
    }
    var c = inFuorigioco(t);
    if (c !== false && Math.abs(c) > gameconf.cusciOffside) {
        if (i == 1) c -= gameconf.cusciOffside;
        else c += gameconf.cusciOffside;
        cambia_missione(t, "rientra", [t.body.x, t.body.y + c])
    }
    return n
}

function azioni_AI(e) {
    if (e.ME.missione != "") return false;
    var t = e.ME.squadra;
    var n = gamevars.Squadre[t].campo;
    var r = e.ME.halapalla;
    if (r) {
        var i = cerca_omino(e);
        if (e.ME.ruolo == "P") {
            tiro(e, "spazza", false);
            return false
        } else {
            var s = incursione_cerca_destinazione(e, i);
            var o = s[0];
            var u = s[1];
            if (o == "vai") {
                vaiverso(e, u);
                return true
            } else if (o == "tiro") {
                tiro(e, "tiro", u);
                return false
            } else if (o == "cross") {
                tiro(e, "cross", u);
                return false
            } else if (o == "pass") {
                var a = check_traiettoria(e, oggetti.calciatori[i]);
                if (a) tiro(e, "pass", i);
                else tiro(e, "pass2", i);
                return false
            } else {
                tiro(e, "tiro", false);
                return false
            }
        }
    } else {
    }
    return false
}

function check_traiettoria(e, t) {
    var n = new Phaser.Line(e.x, e.y, t.x, t.y);
    var r;
    var i;
    var s;
    var o = e.ME.squadra;
    for (var u in oggetti.calciatori) {
        if (o == oggetti.calciatori[u].ME.squadra) continue;
        i = new Phaser.Point;
        s = new Phaser.Point;
        r = new Phaser.Circle(oggetti.calciatori[u].body.x, oggetti.calciatori[u].body.y, gameconf.ominoInfluenza);
        if (intersection(n, r, i, s, true) > 0) {
            return false
        }
    }
    return true
}

function incursione_cerca_destinazione(e, t) {
    var n, r;
    var i = e.ME.squadra;
    var s = gamevars.Squadre[i].campo;
    if (isZonaCross(e)) {
        n = "cross";
        r = false
    } else if (is_zonaTiro(e)) {
        n = "tiro";
        r = false
    } else if (e.ME.avversarimarcati == 0 && SpallePorta(e)) {
        n = "vai";
        r = new Phaser.Point(oggetti.porte[get_opponent_id(s)].centerX, oggetti.porte[get_opponent_id(s)].centerY)
    } else if (e.ME.avversarivicini == 0 || e.ME.avversarimarcati == 0 && e.ME.avversarivicinidavanti == 0) {
        n = "vai";
        r = new Phaser.Point(oggetti.porte[get_opponent_id(s)].centerX, oggetti.porte[get_opponent_id(s)].centerY)
    } else if (t !== false) {
        n = "pass";
        r = t
    }
    return [n, r]
}

function trova_informazioni_giocatori(e) {
    var t, n;
    var r = 0;
    var i = 0;
    var s = 0;
    var o = 0;
    var u = 0;
    var a = e.ME.squadra;
    var f = gamevars.Squadre[a].campo;
    var l = new Phaser.Point(e.body.x, e.body.y);
    for (var c in oggetti.calciatori) {
        if (oggetti.calciatori[c].ME.id == e.ME.id) continue;
        t = l.distance(oggetti.calciatori[c]);
        n = oggetti.calciatori[c].ME.squadra != e.ME.squadra;
        if (t < gameconf.distPressi) {
            if (n) {
                i++;
                if (f == 1 && oggetti.calciatori[c].body.y > l.y || f == 2 && oggetti.calciatori[c].body.y < l.y) s++
            } else r++
        }
        if (t < gameconf.distMarcati) {
            if (n) {
                o++
            }
        }
        if (n && oggetti.calciatori[c].ME.halapalla && t < gameconf.distTackle) u++
    }
    e.ME.compagnivicini = r;
    e.ME.avversarivicini = i;
    e.ME.avversarivicinidavanti = s;
    e.ME.avversarimarcati = o;
    e.ME.cantackle = u
}

function get_opponent_id(e) {
    if (e == 1) return 2;
    else return 1
}

function inFuorigioco(e) {
    if (!oggetti.lineaFuorigioco1 || !oggetti.lineaFuorigioco2 || oggetti.lineaFuorigioco1.y == 0 || oggetti.lineaFuorigioco2.y == 0) return false;
    var t = e.ME.squadra;
    var n = gamevars.Squadre[t].campo;
    if (n == 1 && e.body.y > oggetti.lineaFuorigioco2.y || n == 2 && e.body.y < oggetti.lineaFuorigioco1.y) {
        if (n == 1) {
            return oggetti.lineaFuorigioco2.y - e.body.y
        } else {
            return oggetti.lineaFuorigioco1.y - e.body.y
        }
    } else return false
}

function isActivePlayer(e) {
    var t = e.ME.squadra;
    var n = gamevars.Squadre[t].activePlayer;
    return e.ME.id == oggetti.calciatori[n].ME.id
}

function SpallePorta(e) {
    var t = e.ME.squadra;
    var n = gamevars.Squadre[t].campo;
    var r = get_direzione(e);
    return n == 2 && r.indexOf("Down") > -1 || n == 1 && r.indexOf("Up") > -1
}

function isZonaCross(e) {
    var t = e.ME.squadra;
    var n = gamevars.Squadre[t].campo;
    return (n == 1 && e.body.y > oggetti.aree[2].top || n == 2 && e.body.y < oggetti.aree[1].bottom) && (e.body.x < oggetti.aree[1].left || e.body.x > oggetti.aree[1].right)
}

function is_zonaTiro(e) {
    var t = get_fuoricampo_coords();
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    if (r == 1 && e.body.y > 3200 || r == 2 && e.body.y < 800) return true;
    else return false
}

function get_zonaCampo(e) {
    var t = get_fuoricampo_coords();
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    if (r == 1 && e.body.y > t["MedY"] || r == 2 && e.body.y < t["MedY"]) return "A";
    else return "D"
}

function inAttacco(e) {
    var t = get_fuoricampo_coords();
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    if (r == 1 && e.body.y > t["MedY"] + gameconf.arenaEffettiveH / 4 || r == 2 && e.body.y < t["MedY"] - gameconf.arenaEffettiveH / 4) return true;
    else return false
}

function get_posizione_attivo(e) {
    var t = oggetti.palla.body.x;
    var n = oggetti.palla.body.y;
    return [t, n]
}

function get_posizione_init(e) {
    var t = new Phaser.Point(game.world.centerX, game.world.centerY);
    var n = get_posizione_ruolo(e, t);
    var r = get_fuoricampo_coords();
    var i = r["MedY"];
    var s = e.ME.squadra;
    var o = gamevars.Squadre[s].campo;
    if (o == 1 && n[1] > i || o == 2 && n[1] < i) n[1] = i;
    return n
}

function inverti_zona(e) {
    return confTactics.quanteZoneCampoX * confTactics.quanteZoneCampoY + 1 - e
}

function tatticheCoord2CampoCoord(e, t, n) {
    var r = get_fuoricampo_coords();
    var i = t * gameconf.arenaEffettiveW / confTactics.campoW;
    var s = n * gameconf.arenaEffettiveH / confTactics.campoH;
    if (e == 2) {
        i = gameconf.arenaEffettiveW - i;
        s = gameconf.arenaEffettiveH - s
    }
    i += r["x1"];
    s += r["y1"];
    if (e == 1) {
        i += gameconf.ominoW;
        s += gameconf.ominoH
    } else {
        i -= gameconf.ominoW;
        s -= gameconf.ominoH
    }
    return [i, s]
}

function get_posizione_ruolo(e, t) {
    var n = e.ME.squadra;
    var r = gamevars.Squadre[n].campo;
    var i = gamevars.Squadre[n].tattica;
    if (t === false) t = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
    var s = 0;
    for (var o in oggetti.zone) {
        if (oggetti.zone[o].contains(t.x, t.y)) {
            s = o;
            break
        }
    }
    if (s == 0) s = oggetti.palla.ME.zona;
    if (r == 2) {
        s = inverti_zona(s)
    }
    var u = i[s][e.ME.numero];
    if (u) return tatticheCoord2CampoCoord(r, u.x, u.y);
    else return get_posizione_ruolo_palla(e, t)
}

function get_posizione_ruolo_palla(e, t) {
    var n = get_fuoricampo_coords();
    var r = {
        DifSpostaPallaDifY: -550,
        DifSpostaPallaAttY: -200,
        CCSpostaPallaDifY: -150,
        CCSpostaPallaAttY1: 250,
        CCSpostaPallaAttY2: 300,
        AlaSpostaPallaAttY: 400,
        AttSpostaPallaDifY: 0,
        AttSpostaPallaAttY: 500,
        DifOltreMediana: 0,
        TerziniAddAtt: 150,
        AlaDistLinea: gameconf.arenaEffettiveW / 6,
        SpostaMezzoCentroX: parseInt(gameconf.arenaEffettiveW / 10)
    };
    var i;
    if (gamevars.lastContactID) {
        var s = get_i_from_id(gamevars.lastContactID);
        var o = oggetti.calciatori[s];
        i = o.ME.squadra
    } else i = 0;
    if (t === false) t = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
    var u = e.ME.ruolo;
    var a = e.ME.squadra;
    var f = gamevars.Squadre[a].campo;
    var l, c, h;
    var p = spostaY = 0;
    if (u == "P") {
        if (f == 1) c = oggetti.porte[f].y + gameconf.ominoH;
        else c = oggetti.porte[f].y - gameconf.ominoH
    } else if (u.substr(0, 1) == "D") {
        if (a == i) h = r.DifSpostaPallaAttY;
        else h = r.DifSpostaPallaDifY;
        if (f == 2) h = h * -1;
        c = t.y + h;
        if (f == 1 && c > n["MedY"] + r.DifOltreMediana + 32 || f == 2 && c < n["MedY"] - r.DifOltreMediana - 32) {
            if (f == 1) c = n["MedY"] + r.DifOltreMediana;
            else c = n["MedY"] - r.DifOltreMediana;
            if (u.substr(1, 1) == "D" || u.substr(1, 1) == "S") {
                if (f == 1) c += r.TerziniAddAtt;
                else c -= r.TerziniAddAtt
            }
        } else if (f == 1 && oggetti.palla.body.y > oggetti.aree[1].bottom || f == 2 && oggetti.palla.body.y < oggetti.aree[2].top) {
            if (f == 1 && c < oggetti.aree[1].bottom) c = oggetti.aree[1].bottom;
            else if (f == 2 && c > oggetti.aree[2].top) c = oggetti.aree[2].top
        }
    } else if (u.substr(0, 1) == "C") {
        if (a == i) {
            if (u.substr(1, 1) == "D" || u.substr(1, 1) == "S") {
                h = r.AlaSpostaPallaAttY
            } else {
                if (f == 1 && oggetti.palla.body.y < n["MedY"] || f == 2 && oggetti.palla.body.y > n["MedY"]) h = r.CCSpostaPallaAttY2;
                else h = r.CCSpostaPallaAttY1
            }
        } else h = r.CCSpostaPallaDifY;
        if (f == 2) h = h * -1;
        c = t.y + h;
        if (a == i) {
            if (f == 2 && c < n["y1"] + gameconf.areeH) c = n["y1"] + gameconf.areeH;
            else if (f == 1 && c > n["y2"] - gameconf.areeH) c = n["y2"] - gameconf.areeH
        }
    } else if (u.substr(0, 1) == "A") {
        if (a == i) h = r.AttSpostaPallaAttY;
        else h = r.AttSpostaPallaDifY;
        if (f == 2) h = h * -1;
        c = t.y + h;
        if (f == 1 && c < n["MedY"] - 32 || f == 2 && c > n["MedY"] + 32) c = n["MedY"]
    }
    if (u == "P") {
        l = oggetti.porte[f].centerX
    } else if (u.substr(1, 1) == "C") {
        l = n["MedX"]
    } else if (u.substr(1, 1) == "D" || u.substr(1, 1) == "S") {
        if (u.substr(1, 1) == "D" && f == 1 || u.substr(1, 1) == "S" && f == 2) l = n["x1"] + r.AlaDistLinea;
        else l = n["x2"] - r.AlaDistLinea
    }
    if (u.substr(2, 1) == "D" || u.substr(2, 1) == "S") p = r.SpostaMezzoCentroX;
    if (u.substr(2, 1) == "S" && f == 1 || u.substr(2, 1) == "D" && f == 2) p = p * -1;
    if (p != 0) l += p;
    if (spostaY != 0) c += spostaY;
    var d = torna_in_campo(l, c);
    l = d[0];
    c = d[1];
    return [l, c]
}

function esci_da_zona(e, t, n, r) {
    if (t.ME.ruolo == "P") return n;
    var i = t.ME.squadra;
    var s = gamevars.Squadre[i].campo;
    var o = gamevars.Squadre[gamevars.possesso].campo;
    if (e == 3) {
        if (o == 1 && n[1] < r.bottom || o == 2 && n[1] > r.top) {
            if (o == 1) n[1] = r.bottom;
            else n[1] = r.top
        }
    } else {
        var u = new Phaser.Point(n[0], n[1]);
        if (gamevars.possesso != i && contiene(r, u)) {
            if (s == 1 && n[1] > r.top) n[1] = r.top;
            else if (s == 2 && n[1] < r.bottom) n[1] = r.bottom
        }
    }
    return n
}

function torna_in_campo(e, t) {
    var n = get_fuoricampo_coords();
    if (t < n["y1"]) t = n["y1"];
    else if (t > n["y2"]) t = n["y2"];
    if (e < n["x1"]) e = n["x1"];
    else if (e > n["x2"]) e = n["x2"];
    return [e, t]
}

function get_fuoricampo_coords() {
    var e = (gameconf.arenaW - gameconf.arenaEffettiveW) / 2;
    var t = (gameconf.arenaH - gameconf.arenaEffettiveH) / 2;
    var n = gameconf.arenaW - e;
    var r = gameconf.arenaH - t;
    t += 10;
    r += 10;
    var i = gameconf.arenaEffettiveW / 2 + e;
    var s = gameconf.arenaEffettiveH / 2 + t;
    var o = new Array;
    o["x1"] = e;
    o["x2"] = n;
    o["y1"] = t;
    o["y2"] = r;
    o["MedX"] = i;
    o["MedY"] = s;
    return o
}

function gol(e) {
    var t = get_i_from_id(gamevars.lastContactID);
    var n = oggetti.calciatori[t];
    var r = "";
    if (n.ME.squadra == e) {
        n.ME.animazione = "esulta";
        r = " ** GOAL! ** "
    } else {
        r = " ** OWN-GOAL! ** "
    }
    game.camera.follow(n);
    gamevars.Squadre[e].gol++;
    gamevars.possesso = get_opponent_id(e);
    palla_al_centro();
    set_scena(2);
    show_score();
    oggetti.scritte.giocatore.setText(n.ME.numero + ". " + nome_calciatore(n.ME.db["nome"]) + r);
    tifosi("goal", e)
}

function tifosi(e, t) {
    if (e == "goal") {
        if (t == gamevars.casa) play_sound("goal1");
        else play_sound("goal2")
    } else if (e == "fallo") {
        play_sound("ohh1")
    } else {
        var n = get_fuoricampo_coords();
        var r;
        if (gamevars.lastContactID) {
            var i = get_i_from_id(gamevars.lastContactID);
            var s = oggetti.calciatori[i];
            r = s.ME.squadra
        } else r = 0;
        var o = "";
        if (oggetti.palla.body.y > n["MedY"] && gamevars.Squadre[gamevars.casa].campo == 1 || oggetti.palla.body.y < n["MedY"] && gamevars.Squadre[gamevars.casa].campo == 2) {
            if (r == gamevars.casa) o = "attacca"
        } else {
            if (r != gamevars.casa) o = "difende";
            else o = "imposta"
        }
        if (o == "attacca") suoni["stadio"].volume += .1;
        else suoni["stadio"].volume -= .05;
        if (suoni["stadio"].volume > 2) suoni["stadio"].volume = 2;
        else if (suoni["stadio"].volume < .2) {
            suoni["stadio"].volume = .2;
            if (Math.floor(Math.random() * 10 + 1) == 1) {
                if (gamevars.Squadre[gamevars.casa].gol <= gamevars.Squadre[get_opponent_id(gamevars.casa)].gol) play_sound("fischi");
                else play_sound("coro1");
                suoni["stadio"].volume = .6
            }
        }
    }
}

function palla_al_centro() {
    oggetti.palla.body.setZeroVelocity();
    oggetti.palla.body.x = game.world.centerX;
    oggetti.palla.body.y = game.world.centerY
}

function reset_posizioni() {
    var e;
    for (var t in oggetti.calciatori) {
        e = get_posizione_init(oggetti.calciatori[t]);
        oggetti.calciatori[t].body.x = parseInt(e[0]);
        oggetti.calciatori[t].body.y = parseInt(e[1])
    }
}

function show_score() {
    var e = Database["squadre"][Settings.Squadre[1].id]["nome"];
    var t = Database["squadre"][Settings.Squadre[2].id]["nome"];
    oggetti.scritte.tabellone.setText(e + " " + gamevars.Squadre[1].gol + " - " + t + " " + gamevars.Squadre[2].gol)
}

function fine_frazione() {
    gamevars.timerPartita.pause();
    Punto = new Phaser.Point(50, game.world.centerY);
    set_scena(6, Punto)
}

function fine_partita() {
    if (gamevars.frazionePartita == 1) {
        gamevars.frazionePartita = 2;
        var e = gamevars.Squadre[1].campo;
        var t = gamevars.Squadre[2].campo;
        gamevars.Squadre[1].campo = t;
        gamevars.Squadre[2].campo = e;
        gamevars.possesso = 2;
        set_scena(2)
    } else {
        gamevars.gamestate = 3;
        goto("home")
    }
}

function set_scena(e, t, n) {
    if (e > 0) {
        gamevars.gamestate = 2;
        oggetti.ScenaMainTimer = game.time.create(false);
        oggetti.ScenaMainTimer.start();
        if (e == 1) {
            oggetti.palla.body.x = game.world.centerX;
            oggetti.palla.body.y = game.world.centerY;
            game.camera.follow(oggetti.calciatori[oggetti.calciatori.length - 1]);
            for (var r in oggetti.calciatori) {
                oggetti.calciatori[r].ME.missione = "run";
                oggetti.calciatori[r].ME.missioneX = oggetti.calciatori[r].body.x + gameconf.arenaW / 2;
                oggetti.calciatori[r].ME.missioneY = oggetti.calciatori[r].body.y;
                gamevars.Squadre[oggetti.calciatori[r].ME.squadra].activePlayer = r
            }
        } else if (e == 6) {
            for (var r in oggetti.calciatori) {
                oggetti.calciatori[r].ME.missione = "run";
                oggetti.calciatori[r].ME.missioneX = t.x;
                oggetti.calciatori[r].ME.missioneY = t.y
            }
            game.camera.follow(oggetti.calciatori[gamevars.Squadre[1].activePlayer])
        } else {
            var i = new Phaser.Point(game.world.centerX, game.world.centerY);
            if (e == 2) {
                t = i
            }
            if (typeof n === "undefined") n = new Phaser.Circle(t.x, t.y, gameconf.diametroCC);
            var s, o, u, a, f, l, c, h;
            var p = false;
            for (var r in oggetti.calciatori) {
                a = oggetti.calciatori[r].ME.squadra;
                f = gamevars.Squadre[a].campo;
                c = userTeam(oggetti.calciatori[r]);
                l = "run";
                if (e == 2) s = get_posizione_init(oggetti.calciatori[r]);
                else {
                    if (e == 3) h = i;
                    else h = t;
                    s = get_posizione_ruolo(oggetti.calciatori[r], h)
                }
                if (p === false && (e == 2 && oggetti.calciatori[r].ME.ruolo.indexOf("A") > -1 && a == gamevars.possesso || e == 3 && oggetti.calciatori[r].ME.ruolo == "P" && a == gamevars.possesso || e > 3 && r == gamevars.Squadre[a].activePlayer && a == gamevars.possesso)) {
                    p = true;
                    s = [t.x, t.y];
                    l = "kick";
                    gamevars.lastContactID = oggetti.calciatori[r].ME.id
                }
                if (l.substr(0, 4) != "kick") {
                    s = esci_da_zona(e, oggetti.calciatori[r], s, n)
                }
                s = torna_in_campo(s[0], s[1]);
                o = s[0];
                u = s[1];
                cambia_missione(oggetti.calciatori[r], l, [o, u])
            }
        }
        if (typeof n !== "undefined") oggetti.zonafranca = n
    } else {
        oggetti.ScenaMainTimer.destroy();
        game.camera.follow(oggetti.palla);
        camera_set_deadzone();
        for (var r in oggetti.calciatori) {
            oggetti.calciatori[r].ME.animazione = "";
            cambia_missione(oggetti.calciatori[r], "")
        }
        gamevars.gamestate = 1;
        oggetti.zonafranca = false
    }
    gamevars.scena = e
}

function scena() {
    var e = 0;
    var t = false;
    var n = false;
    var r = false;
    var i = false;
    var s = new Phaser.Point(oggetti.palla.body.x, oggetti.palla.body.y);
    for (var o in oggetti.calciatori) {
        var u = [oggetti.calciatori[o].ME.missioneX, oggetti.calciatori[o].ME.missioneY];
        var a = new Phaser.Point(u[0], u[1]);
        if (!check_inattivo(oggetti.calciatori[o], true)) vaiverso(oggetti.calciatori[o], a);
        else esegui_missione(oggetti.calciatori[o]);
        if (oggetti.calciatori[o].ME.missione == "") {
            e++
        } else if (oggetti.calciatori[o].ME.missione == "done") {
            oggetti.calciatori[o].ME.missione = "";
            t = true;
            break
        } else if (oggetti.calciatori[o].ME.missione == "kickready") {
            n = o
        }
        dettagli_omino(oggetti.calciatori[o]);
        trova_informazioni_giocatori(oggetti.calciatori[o]);
        r = calcola_offside_coords(oggetti.calciatori[o], r);
        i = calcola_active_player(s, o, oggetti.calciatori[o], i)
    }
    calcola_offside_coords2(r);
    var f = get_active_player(s, i);
    focus_omino(f);
    if (gamevars.scena == 1 && e == oggetti.calciatori.length) {
        game.camera.follow(oggetti.palla);
        set_scena(2)
    } else if (n !== false && e == oggetti.calciatori.length - 1) {
        cambia_missione(oggetti.calciatori[n], "kickdo")
    } else if (gamevars.scena == 6 && e == oggetti.calciatori.length) {
        fine_partita()
    } else if (t == true) {
        if (gamevars.scena == 2) inizio_partita();
        else set_scena(0)
    }
}

function inizio_partita() {
    if (gamevars.timerPartita === false) {
        gamevars.timerPartita = game.time.create(false);
        gamevars.timerPartita.loop(1e3, scorri_tempo, this);
        gamevars.timerPartita.start()
    } else gamevars.timerPartita.resume();
    set_scena(0)
}

function scorri_tempo() {
    var e;
    var t = 0;
    if (gamevars.timerPartita === false) {
        e = 0
    } else {
        var n = 5400;
        var r = gameconf.durataPartita;
        t = parseInt(gamevars.timerPartita.ms / 1e3)
    }
    if (t == 0) e = 0;
    else if (gamevars.frazionePartita == 1 && t >= r / 2) {
        fine_frazione();
        e = 45
    } else if (t >= r) {
        fine_frazione();
        e = 90
    } else {
        var i = t * n / r;
        e = parseInt(i / 60) + 1
    }
    oggetti.scritte.tempo.setText(e + "'")
}

function skippa() {
    if (gamevars.scena != 1 && gamevars.scena != 2 && gamevars.scena != 6) return false;
    if (oggetti.ScenaMainTimer.ms < 1e3) return false;
    for (var e in oggetti.calciatori) {
        oggetti.calciatori[e].body.x = oggetti.calciatori[e].ME.missioneX;
        oggetti.calciatori[e].body.y = oggetti.calciatori[e].ME.missioneY
    }
}

function scena_do(e) {
    var t = userTeam(e);
    if (e.ME.missione == "kick") {
        cambia_missione(e, "kickready");
        piazza_palla(e.body.x, e.body.y)
    } else if (e.ME.missione == "kickready") {
        if (gamevars.scena > 2) {
            cambia_missione(e, "kickdo")
        }
    } else if (e.ME.missione == "kickdo") {
        oggetti.palla.ME.timerScena.stop();
        if (t == 0) {
            if (oggetti.ScenaMainTimer.ms > gameconf.ScenaDurMin || gamevars.scena == 2) {
                calcio_da_fermo(e)
            }
        } else {
        }
    } else if (e.ME.missione == "run") e.ME.missione = ""
}

function calcio_da_fermo(e, t) {
    var n = cerca_omino(e, 1);
    if (t == "tiro") tiro(e, "tiro", false);
    else if (t == "pass2") tiro(e, "pass2", n);
    else tiro(e, "pass", n);
    cambia_missione(e, "done")
}
var game, Preferenze, Settings;
var oggetti = {};
var tasti = new Array;
var gamevars = {
    gamestate: 0,
    flags: {}
};
var suoni = new Array