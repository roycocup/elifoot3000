var world_geom = {minX:20, minY:20, maxX:1200, maxY:900}
var pitch_dist = {maxX:1121, maxY:728}
var pitch_geom = {
		minX: 78,
		minY: 90,
		maxX: 78+pitch_dist.maxX,
		maxY: 90+pitch_dist.maxY,
	};
pitch_geom.centerY = (pitch_geom.maxY/2)+(pitch_geom.minY/2);
pitch_geom.centerX = (pitch_geom.maxX/2)+(pitch_geom.minX/2);

// calculations on the coordinates are done by adding the pitch_geom.min x or y.
// Thus this translation is to correct that. Use this for coordinates on players if
// you need to get the center of the pitch.
var translated_pitch_geom = {
	centerY : (pitch_geom.maxY/2)+(pitch_geom.minY/2) - pitch_geom.minY,
	centerX : (pitch_geom.maxX/2)+(pitch_geom.minX/2) - pitch_geom.minX,
};

var game;
var screen = {x: 960, y: 640};
// var screen = {x: world_geom.maxX, y: world_geom.maxY};
function init(){
	game = new Phaser.Game(screen.x, screen.y, Phaser.AUTO, 'gamecanvas',{preload:preload,create:create,update:update,render:render});
}


function preload(){
	game.load.image('pitch', 'assets/images/pitch-clean.jpg');
	game.load.image('player', 'assets/images/player.png');
	game.load.image('player-purple', 'assets/images/player-purple.png');
	game.load.image('ball', 'assets/images/ball.png');
	// game.load.audio('crowd', 'assets/audio/crowd.ogg');
}

var helperRect;
var timer;

var ai = new AI();
var p1H, p9H, p1A, p9A;
var ball;
var players;
var cursors, spaceKey;

function create(){
	// sounds
	// crowd = game.add.audio('crowd');
	// crowd.play();
	cursors = game.input.keyboard.createCursorKeys();
	spaceKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
  spaceKey.onDown.add(togglePause, this);

	// basics
	game.world.setBounds(world_geom.minX, world_geom.minY, world_geom.maxX, world_geom.maxY);
	game.physics.startSystem(Phaser.Physics.ARCADE);

	// timer
	timer = game.time.create(false);
	timer.loop(1000, function(){}, this);
	timer.start();

	// elements
	pitch = game.add.sprite(0,0,'pitch');
	ball = new Ball(game);
	ball.reset();

	// Players
	playersGroup = game.add.group();
	p1H = new Player(game, 'player', {name:'p1H', role:'GK', team:'home'});
	p9A = new Player(game, 'player-purple', {name:'p9A', role:'CS', team:'away'});
	players = [];
	players = [p1H, p9A];
	for(var i=0; i<players.length; i++){
		players[i].reset();
		playersGroup.add(players[i].sprite);
	}
	ai.start();

	// Camera
	game.camera.follow(ball.sprite);

}

function update(){
	for(var i=0; i<players.length; i++){
		players[i].update();
	}
	checkCollisions();
}


function render(){
	//renderGrid();
	// game.debug.text('Player 1 orders: '+JSON.stringify(p9A.order), 100, 100);
	// game.debug.bodyInfo(p9A.sprite, 100, 150);
	// game.debug.text(timer.duration.toFixed(0), 100, 100);
	// helperRect = new Phaser.Rectangle(world_geom.minX, world_geom.minY, world_geom.maxX, world_geom.maxY);
	// game.debug.geom(helperRect);
}

function checkCollisions(){
	game.physics.arcade.collide(ball.sprite, playersGroup, function(ball,player){
		var player = eval(player.mainObj);
		player.grabBall(ball);
	});
}

function togglePause() {
    game.physics.arcade.isPaused = (game.physics.arcade.isPaused) ? false : true;
}

function renderGrid(){
	var step = 100;
	for(var x = pitch_geom.minX; x < pitch_geom.maxX; x += step){
		line = new Phaser.Line(x, pitch_geom.minY, x, pitch_geom.maxY);
		game.debug.geom(line);
		// game.debug.text(x, x-10, pitch_geom.centerY);
	}

	for(var y = pitch_geom.minY; y < pitch_geom.maxY; y += step){
		line = new Phaser.Line(pitch_geom.minX, y, pitch_geom.maxX, y);
		game.debug.geom(line);
	}
}

function checkOverlap(spriteA, spriteB) {
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();
    return Phaser.Rectangle.intersects(boundsA, boundsB);
}

init();
