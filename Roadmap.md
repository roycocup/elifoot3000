# Roadmap
## Definition
This roadmap has the practical goals for a workable demo in 4 weeks

### Phase 1
	[done] 2 teams of 2 players each with initial positioning (Gk +  Generic Player)
	[done] 4 players moving independently within boundaries
	[done] Resetting ball
	[done] Resetting at start
	[done] Ball dribbling
	[] Rotation with ball
	[] AI design (split)
	[] Passing
	[] Scoring
	[] Sounds on collision, out of bounds, score, ambience
	[] Damping on ball velocity
	[] numbers on players
	[] movement with acceleration
	[] movement with decay on players
### Phase 2
	[] Scoreboard
	[] Time and pause
	[] PLayer stats
	[] Pre Game Menu
	[] Team Selection
	[] Tactics
	[] In Game Menu
	[] Reset half-term
	[] Reset game over
	[] corners
	[] offside
	[] caught by keeper
### Phase 3
	[] Defense Collisions (1v1)
	[] Sprinting 1v1
	[] Headers and bouncers
### Phase 3
	[] Defense Collisions/decisions
	[] Backend connection
### Week 4
	[] Better Textures
	[] Clean up
	[] Test mobile
	[] Publish on website for desktop
	[] Optimize assets into "single pulls" Atlas and Audios
