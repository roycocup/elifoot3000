var Ball = function(game, possession)
{
	var x = 0;
	var y = 0;

	this.game = game;
	this.possession = {};
	this.sprite = game.add.sprite(x, y, 'ball');
	this.sprite.mainObj = this;
	this.sprite.anchor.set(0.5);
	game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

	// this.sprite.body.collideWorldBounds = true;
	// this.sprite.body.immovable = true;
	// this.sprite.body.damping = 0.5;
	// this.sprite.body.mass = 0.5;
	this.sprite.body.drag = 0.1;

}

Ball.prototype = {
	kicked: function(direction, speed){
		// game.physics.arcade.moveToXY(this.sprite, direction.x, direction.y, speed);
		this.sprite.angle = game.physics.arcade.accelerateToXY(this.sprite, direction.x, direction.y, speed, speed, speed);
		// friction
		setInterval(function(){
			var frictionFactor = 0.25;
			var min_speed = 10;
			this.sprite.body.acceleration.y -= this.sprite.body.acceleration.y * frictionFactor;
			this.sprite.body.acceleration.x -= this.sprite.body.acceleration.x * frictionFactor;
			if (this.sprite.body.acceleration.x < min_speed || this.sprite.body.acceleration.y < min_speed){
				this.sprite.body.acceleration.set(0);
				this.sprite.body.velocity.set(0);
			}
		}.bind(this), 200);
	},
	reset : function(x, y){
		if (!x)
			x = pitch_geom.centerX;
		if (!y)
			y = pitch_geom.centerY;
		this.sprite.reset(x,y);
	},
}
