var AI = function()
{

  this.start = function(){
    var ball_coors = this.findBall();
    var players_info = this.getPlayersInfo();
    //dev only
    this.giveOrdersToPlayer();
  }

  this.runToBall = function(){}

  this.patrolArea = function(){}

  this.getOrders = function(){
    var orders = [
      {x: translated_pitch_geom.centerX, y: translated_pitch_geom.centerY},
      {x: 100, y: 100, kick:{direction: new Phaser.Point(100,100), speed: 250}},
      {x: 250, y: 500},
      {x: 250, y: 550},
      {x: 300, y: 600},
      {x: translated_pitch_geom.centerX, y: translated_pitch_geom.centerY},
    ];
    return orders;
  }

  this.giveOrdersToPlayer = function(){
    var orders = this.getOrders();
    // add the actual sizes from field to individual orders
    for (i in orders){
      orders[i].x = pitch_geom.minX + orders[i].x;
      orders[i].y = pitch_geom.minY + orders[i].y;
      p9A.addOrder(orders[i]);
    }
  	p9A.restart = true;
  }

  this.findBall = function(){
    return {x:ball.sprite.x, y:ball.sprite.y};
  }

  this.getPlayersInfo = function(){
    var players_info = [];
    playersGroup.forEach(function(player){
      var player = eval(player.name);
      players_info.push({x:player.sprite.x, y:player.sprite.y, possession:player.possession, role:player.role});
    });
    return players_info;
  }

}
