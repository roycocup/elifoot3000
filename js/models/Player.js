var Player =  function(game, skin, options)
{
	var x = 0;
	var y = 0;

	this.game = game;
	this.sprite = game.add.sprite(x, y, skin);
	this.sprite.name = options.name;
	this.sprite.mainObj = this;
	this.sprite.anchor.set(0.5);
	game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

	this.sprite.body.collideWorldBounds = true;
	this.sprite.body.immovable = true;
	this.sprite.body.allowRotation = true;
	this.sprite.rotation = 1.571;
	this.sprite.body.setCircle(3);

	this.team = options.team;
	this.role = options.role;

};

Player.prototype = {
	// order chain
	orders : [],
	// current order
	order : {},
	// when starting or restarting game, need to execute the first order in the chain
	restart : false,
	// this is a flag for completion without which the next order is not going into execution
	orderCompleted : false,

	team : undefined, // home - left side 1st half
	role : undefined,
	possession : false,
	kicking: false,

	addOrder : function(order){
		this.orders.push(order);
	},
	update : function (){
		// if we need a new order or if this is the very beginning of orders
		if(this.orderCompleted || this.restart){
			this.restart = false;
			if (this.orders.length > 0){
				this.order = this.orders.shift();
				this.execute();
			}
		}
		// while moving
		var distance = game.physics.arcade.distanceToXY(this.sprite, this.order.x, this.order.y);
		if (distance < 1){
			this.sprite.body.velocity.set(0);
			this.orderCompleted = true;
		}
	},
	execute : function() {
		// if player is stopped
		if (this.sprite.body.speed == 0){
			this.sprite.body.velocity.set(0);
			this.orderCompleted = false;
			// if theres a kick in the orders
			if (this.order.kick != undefined){
				this.kick(this.order.kick.direction, this.order.kick.speed);
			}
			// and move
			this.move(this.order.x, this.order.y, 100);
		}
	},
	move : function(x, y, speed){
		this.sprite.pivot.x = 0;
		//rotate towards direction
		this.rotateToDest(new Phaser.Point(x,y));
		game.physics.arcade.moveToXY(this.sprite, x, y, speed);
	},
	grabBall: function(ball){
		if (!this.kicking){
			ball.body.velocity.set(0);
			ball.x = -12; // radius of player + 2px
			ball.y = 0;
			this.sprite.addChild(ball);
			this.possession = true;
		}
	},
	kick: function(direction, speed){
		var ball = this.sprite.getChildAt(0);
		ball.mainObj.kicked(direction, speed);
		this.kicking = true;
		// cancel kicking after x milliseconds
		setTimeout(function(){
			this.kicking = false;
		}.bind(this), 300);
		// must remove child from its sprite
		// this.sprite.removeChildAt(0);
		this.possession = false;
	},
	rotateToDest : function(p) {
		var dir_angle = this.game.physics.arcade.angleBetween(p, this.sprite)
		this.sprite.rotation = dir_angle;
	},
	reset : function(){
		var x;
		var y;
		switch (this.role){
			case 'GK':
				var dist_goal = 50
				if (this.team == 'home'){
					x = pitch_geom.minX + dist_goal;
					y = pitch_geom.centerY;
				}else{
					x = pitch_geom.maxX - dist_goal;
					y = pitch_geom.centerY;
				}
				break;
			case 'LB':
			case 'LCB':
			case 'SW':
			case 'CB':
			case 'RB':
			case 'RCB':

			case 'LWB':
			case 'LDM':
			case 'CDM':
			case 'RWB':
			case 'RDM':

			case 'LM':
			case 'LCM':
			case 'CM':
			case 'RCB':
			case 'RM':

			case 'LW':
			case 'LAM':
			case 'CAM':
			case 'RAM':
			case 'RW':

			case 'LS':
			case 'CS':
			case 'RS':
				var dist_center = 30;
				if (this.team == 'home'){
					x = pitch_geom.centerX - dist_center;
					y = pitch_geom.centerY;
				} else {
					x = pitch_geom.centerX + dist_center;
					y = pitch_geom.centerY;
				}

				break;
		}
		this.sprite.reset(x,y);
	},
}
